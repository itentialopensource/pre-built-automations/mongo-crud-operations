
## 0.2.9 [06-21-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!28

---

## 0.2.8 [06-21-2022]

* patch/DSUP-1361

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!27

---

## 0.2.7 [11-22-2021]

* Patch/dsup 1046

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!26

---

## 0.2.6 [10-05-2021]

* Removed app-form_builder IAPDependency from package.json

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!25

---

## 0.2.5 [07-26-2021]

* Update workflow to work with mongo adapter change

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!23

---

## 0.2.4 [07-02-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!21

---

## 0.2.3 [01-25-2021]

* Added instance data

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!20

---

## 0.2.2 [01-25-2021]

* Removed defaults from JSON forms.

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!19

---

## 0.2.1 [01-25-2021]

* Update to the delete workflow

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!18

---

## 0.2.0 [06-18-2020]

* [minor/LB-404] Add './' to img path

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!16

---

## 0.1.0 [06-17-2020]

* Update manifest to reflect gitlab naming

See merge request itentialopensource/pre-built-automations/Mongo-CRUD-Operations!15

---

## 0.0.6 [05-01-2020]

* patch/README.md - Update

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!14

---

## 0.0.5 [04-17-2020]

* Update package.json

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!13

---

## 0.0.4 [04-17-2020]

* Update package.json

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!13

---

## 0.0.3 [04-17-2020]

* Update package.json

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!13

---

## 0.0.2 [04-15-2020]

* Updated IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/mongo-crud-operations!12

---\n
